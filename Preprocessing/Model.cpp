#include "Model.h"
#include "Parser.h"
#include "Logger.h"
#include <fstream>
#include <sstream>

Model::Model(const std::string &file_uri, unsigned d) {
	delta = d;
	Parser parser(file_uri);
	file_name = parser.getFileName(); 
	std::ifstream file(file_name+"Maps.dat");
	if(!file.good()) {
		Logger::message("file.dat non presente");
		parser.initialize(tensor, mapper);
		unsigned long sizes[3];
		for(int i=1; i<4; i++)
			sizes[i-1] = mapper.getSize(i);
		tensor.set(sizes, d);
	}
}

void Model::save() {
	std::ifstream file("part1.dat", std::ios::binary);
	if(!file.good()) {
		Logger::message("crea la struttura octTree");
		tensor.organizeData();
		Logger::message("salva la struttura");
		tensor.save(file_name);
	}
	mapper.save(file_name);
}
