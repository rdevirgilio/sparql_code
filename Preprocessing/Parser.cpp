#include "Parser.h"
#include "Logger.h"
#include <fstream>
#include <raptor2.h>
#include <stdio.h>

#ifdef WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

static TensorSPARQL tensor_copy;
static Mapper maps;

void handle_triples(void* out, raptor_statement* triple) {
	Tuple t;
	std::string subject = (const char*) raptor_term_to_string(triple->subject);
	std::string predicate = (const char*) raptor_term_to_string(triple->predicate);
	std::string object = (const char*) raptor_term_to_string(triple->object);
	if(subject.at(0) == '<')
		subject = subject.substr(1,subject.length()-2);
	if(predicate.at(0) == '<')
		predicate = predicate.substr(1,predicate.length()-2);
	if(object.at(0) == '<')
		object = object.substr(1,object.length()-2);
	t.setValue(SUBJECT, maps.addSubject(subject));
	t.setValue(PREDICATE, maps.addPredicate(predicate));
	t.setValue(OBJECT, maps.addObject(object));
	tensor_copy.add(t);
}

Parser::Parser(const std::string &uri) {
	file_uri = uri;
	int file_start = file_uri.find_last_of("/");
	if(file_start == std::string::npos) {
		data_file = uri;
		char currentDir[FILENAME_MAX];
		GetCurrentDir(currentDir, sizeof(currentDir));
		file_uri = "file:///"+std::string(currentDir)+"/data/"+uri;
	}
	else data_file = file_uri.substr(file_start+1);
	int ext_point = data_file.find_last_of(".");
	if(ext_point!=std::string::npos) {
		std::string extension = data_file.substr(ext_point+1);
		Logger::message("l'estensione del file e' "+extension);
		if(extension == "nt")
			name_parser = "ntriples";
		else if(extension == "ttl")
			name_parser = "turtle";
		else if(extension == "RDF" || extension == "rdf")
			name_parser = "rdfxml";
	}
	else Logger::message("file extension error");
}

std::string Parser::getFileName() {
	int ext_point = data_file.find(".");
	if(ext_point!=std::string::npos)
		return data_file.substr(0,ext_point);
	else {
		Logger::message("file name error");
		return "";
	}
}

void Parser::initialize(TensorSPARQL &t, Mapper &mapper) {
	int ext_point = data_file.find_last_of(".");
	if(ext_point==std::string::npos) {
		Logger::message("data file name error");
		return;
	}
	raptor_world* world = NULL;
	raptor_parser* parser = NULL;
	unsigned char* uri_string;
	raptor_uri *uri, *base_uri;

	world = raptor_new_world();
	Logger::message("parser "+name_parser+" creato");
	parser = raptor_new_parser(world, name_parser.c_str());
	raptor_parser_set_statement_handler(parser, NULL, handle_triples);

	uri_string = (unsigned char*) file_uri.c_str();
	uri = raptor_new_uri(world, uri_string);
	base_uri = raptor_uri_copy(uri); 

	raptor_parser_parse_file(parser, uri, base_uri);
	raptor_free_uri(uri);
	raptor_free_uri(base_uri);
	raptor_free_parser(parser);
	raptor_free_world(world); 
	Logger::message("fine caricamento");
	t = tensor_copy;
	mapper = maps;
}