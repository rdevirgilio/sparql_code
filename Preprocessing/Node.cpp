#include "Node.h"
#include <sstream>
#include <fstream>
#include <math.h>

void fill(unsigned long* sizes, unsigned long minX, unsigned long minY, unsigned long minZ,
		   unsigned long maxX, unsigned long maxY, unsigned long maxZ) {
			   sizes[0] = minX; sizes[1] = minY; sizes[2] = minZ;
			   sizes[3] = maxX; sizes[4] = maxY; sizes[5] = maxZ;
}

void Node::getSizesForNNode(unsigned long* sizes, int index) {
	unsigned long centre[3];
	for(int i=0; i<3; i++)
		centre[i] = ceil((maxSizes[i]+minSizes[i])/2.0);
	switch (index) {
	case GGG:
		fill(sizes, centre[0], centre[1], centre[2], maxSizes[0], maxSizes[1], maxSizes[2]); break;
	case GGL:
		fill(sizes, centre[0], centre[1], minSizes[2], maxSizes[0], maxSizes[1], centre[2]); break;
	case GLG:
		fill(sizes, centre[0], minSizes[1], centre[2], maxSizes[0], centre[1], maxSizes[2]); break;
	case GLL:
		fill(sizes, centre[0], minSizes[1], minSizes[2], maxSizes[0], centre[1], centre[2]); break;
	case LGG:
		fill(sizes, minSizes[0], centre[1], centre[2], centre[0], maxSizes[1], maxSizes[2]); break;
	case LGL:
		fill(sizes, minSizes[0], centre[1], minSizes[2], centre[0], maxSizes[1], centre[2]); break;
	case LLG:
		fill(sizes, minSizes[0], minSizes[1], centre[2], centre[0], centre[1], maxSizes[2]); break;
	case LLL:
		fill(sizes, minSizes[0], minSizes[1], minSizes[2], centre[0], centre[1], centre[2]); break;
	default:
		Logger::message("Node->getSizesForNNode: caso non gestito");
	}
}

void Node::splitNode() {
	int id = nodeID;
	nodeID = 0;
	for(int i=0; i<8; i++) {
		Node node;
		unsigned long sizes[6];
		getSizesForNNode(sizes, i);
		if(i == 0)
			node.setNode(sizes, delta, id);
		else {
			node.setNode(sizes, delta, node_ID);
			node_ID++;
		}
		nodes.push_back(node);
	}
}

Partitions Node::getIndex(const Tuple &t) {
	unsigned long centreX = ceil((maxSizes[0]+minSizes[0])/2.0);
	unsigned long centreY = ceil((maxSizes[1]+minSizes[1])/2.0);
	unsigned long centreZ = ceil((maxSizes[2]+minSizes[2])/2.0);
	if(t[SUBJECT] >= centreX) {
		if(t[PREDICATE] >= centreY)
			if(t[OBJECT] >= centreZ)
				return GGG;
			else return GGL;
		else if(t[OBJECT] >= centreZ)
			return GLG;
		else return GLL;
	}
	else {
		if(t[PREDICATE] >= centreY) 
			if(t[OBJECT] >= centreZ)
				return LGG;
			else return LGL;
		else if(t[OBJECT] >= centreZ)
			return LLG;
		else return LLL;
	}
}

Node::Node() {
	minSizes = new unsigned long[3];
	maxSizes = new unsigned long[3];
}

void Node::setNode(unsigned long* sizes, unsigned d, int ID) {
	minSizes[0] = sizes[0]; minSizes[1] = sizes[1]; minSizes[2] = sizes[2];
	maxSizes[0] = sizes[3]; maxSizes[1] = sizes[4]; maxSizes[2] = sizes[5];
	nodeID = ID;
	delta = d;
}

void Node::add(const Tuple &t) {
	std::stringstream ss;
	ss << nodeID;
	if(nodeID == 0) {
		Partitions index = getIndex(t);
		nodes[index].add(t); 
	}
	else {
		tuples.push_back(t);
		if(tuples.size() > delta) {
			splitNode();
			for(unsigned i=0; i<tuples.size(); i++) {
				Partitions index = getIndex(tuples[i]);
				nodes[index].add(tuples[i]);
			}
			tuples.clear();
		}
	}
}

void Node::save(const std::string &name) {
	if(nodeID == 0)
		for(int i=0; i<8; i++)
			nodes[i].save(name);
	else {
		if(tuples.size() != 0) {
			std::stringstream ss;
			ss << file_number;
			unsigned long sub, pre, obj;
			std::ofstream file;
			file.open(name+"part"+ss.str()+".dat", std::ios::binary);
			unsigned size = tuples.size();
			file.write((char*)&size, sizeof(size));
			for(unsigned i=0; i<size; i++) {
				sub = tuples[i][SUBJECT];
				pre = tuples[i][PREDICATE];
				obj = tuples[i][OBJECT];
				file.write((char*)&sub, sizeof(unsigned long));
				file.write((char*)&pre, sizeof(unsigned long));
				file.write((char*)&obj, sizeof(unsigned long));
			}
			file_number++;
			file.close();
		}
	}
}

int Node::getNodesSize() {
	return node_ID-1;
}