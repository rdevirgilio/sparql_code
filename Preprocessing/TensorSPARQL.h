/**
\class TensorSPARQL

\brief Provide Tensor representation

This class realizes the Tensor representation for containing data.
*/
#ifndef __TENSORSPARQL_H__
#define __TENSORSPARQL_H__

#include <vector>
#include "Node.h"
#include "Tuple.h"

typedef std::pair<unsigned char, unsigned long> Delta;

class TensorSPARQL {
	private:
		unsigned long * dimensions;
		unsigned char rank;
		std::vector<Tuple> NZE;
		Node octTree;
		int size;
		int delta;
		int process;
	public:
		TensorSPARQL();
        /** this method adds dimensions to this Tensor */
		void set(unsigned long * dimensions, int delta);
		/** this function adds a NonZeroElement to this Tensor */
		void add(const Tuple &element);
		/** returns all NonZeroElement of the Tensor */
		std::vector<Tuple> all() const;
		int getSize();
		void organizeData();
		void save(const std::string &name);
		void printAll();
};

#endif
