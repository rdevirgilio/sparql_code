/**
\class Model

\brief Provide representation of the dataset

This class main function is to bind the data representation with the respective mapping
*/
#ifndef __MODEL_H__
#define __MODEL_H__

#include "Mapper.h"
#include "TensorSPARQL.h"
#include <sstream>
#include <vector>

class Model {
	private:
		Mapper mapper;
		TensorSPARQL tensor;
		int delta;
		std::string file_name;
	public:
		/** constructor which take as parameter the uri of the file as a std::string */
		Model(const std::string &file_uri, unsigned delta);
		void save();
};

#endif