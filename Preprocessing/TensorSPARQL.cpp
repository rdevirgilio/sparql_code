#include "TensorSPARQL.h"

TensorSPARQL::TensorSPARQL() {
	process = 1;
	size = 0;
}

void TensorSPARQL::set(unsigned long * dims, int d) {
	delta = d;
	dimensions = dims;
	rank = 0;
	for(short i=0; i<3; i++)
		if(dimensions[i]!=0)
			rank++;
	unsigned long sizes[6];
	sizes[0] = 0; sizes[1] = 0; sizes[2] = 0; 
	sizes[3] = dimensions[0]; sizes[4] = dimensions[1]; sizes[5] = dimensions[2];
	octTree.setNode(sizes, delta, 1);
}

void TensorSPARQL::add(const Tuple &element) {
	NZE.push_back(element);
}

std::vector<Tuple> TensorSPARQL::all() const{
	if(NZE.size() == 0) {
		std::vector<Tuple> result;
		Tuple tuple;
		tuple(0, 0, 0);
		return result;
	}
	else return NZE;
}

int TensorSPARQL::getSize() {
	return NZE.size();
}

void TensorSPARQL::organizeData() {
	for(unsigned i=0; i<NZE.size(); i++) {
		octTree.add(NZE[i]);
	}
}

void TensorSPARQL::save(const std::string &name) {
	int length = name.length();
	octTree.save(name);
}

void TensorSPARQL::printAll() {
	std::vector<Tuple>::iterator it;
	for(it=NZE.begin(); it!=NZE.end(); it++) {
		(*it).print();
	}
	std::cout << std::endl;
}