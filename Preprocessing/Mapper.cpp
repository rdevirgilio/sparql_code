#include "Mapper.h"
#include "Logger.h"
#include "Tuple.h"
#include <map>
#include <string>
#include <iostream>
#include <fstream>

Mapper::Mapper() {
	i = j = k = 1;
}

unsigned long Mapper::addSubject(const std::string &s) {
	unsigned long val = subjects[s];
	if(val == 0) {
		val = i;
		subjects[s] = i;
		i++;
	}
	return val;
}

unsigned long Mapper::addPredicate(const std::string &s) {
	unsigned long val = predicates[s];
	if(val == 0) {
		val = j;
		predicates[s] = j;
		j++;
	}
	return val;
}

unsigned long Mapper::addObject(const std::string &s) {
	unsigned long val = objects[s];
	if(val == 0) {
		val = k;
		objects[s] = k;
		k++;
	}
	return val;
}

unsigned long Mapper::getSize(const char &c) const{
	switch (c) {
	case SUBJECT: return subjects.size();
	case PREDICATE: return predicates.size();
	case OBJECT: return objects.size();
	default: Logger::message("errore nel ritorno delle dimensioni di Mapper"); return 0;
	}
}

void Mapper::save(const std::string &name) {
	std::ofstream file;
	file.open(name+"Maps.dat", std::ios::binary);
	for(int i=1; i<4; i++) {
		std::string nmap = "";
		string2value map;
		switch (i) {
		case SUBJECT: nmap = "subjects"; 
			map = subjects; break;
		case PREDICATE: nmap = "predicates"; 
			map = predicates; break;
		case OBJECT: nmap = "objects"; 
			map = objects; break;
		}
		Logger::message("salvataggio di Map di "+nmap); 
		unsigned msize = map.size();
		string2value::iterator it;
		file.write((char*)&msize, sizeof(msize));
		for(it=map.begin(); it!=map.end(); it++) {
			std::string string = it->first;
			unsigned long value = it->second;
			int length = string.length();
			file.write((char*)&length, sizeof(length));
			file.write(string.c_str(), length);
			file.write((char*)&value, sizeof(value));
		}
	} 
	file.close();
}