#ifndef __NODE_H__
#define __NODE_H__

#include "Tuple.h"
#include <vector>

static int file_number = 1;

enum Partitions { GGG, GGL, GLG, GLL, LGG, LGL, LLG, LLL };
static int node_ID = 2;

class Node {
	private:
		int nodeID;
		std::vector<Node> nodes;
		std::vector<Tuple> tuples;
		unsigned long* maxSizes;
		unsigned long* minSizes;
		unsigned delta;
		void getSizesForNNode(unsigned long* sizes, int index);
		void splitNode();
		Partitions getIndex(const Tuple &t);
	public:
		Node();
		void setNode(unsigned long* sizes, unsigned delta, int ID);
		void add(const Tuple &t);
		void save(const std::string &name);
		int getNodesSize();
};

#endif