/**
\class Mapper

\brief Provide the mapping of subjects, predicates and objects

This class main function is to map string values of subjects, predicate and objects to their
respective long representation used in the model.
*/
#ifndef __MAPPER_H__
#define __MAPPER_H__

#include <string>
#include <map>

typedef std::map<std::string, unsigned long> string2value;

class Mapper {
	private:
		unsigned long i,j,k;
		string2value subjects;
		string2value predicates;
		string2value objects;
	public:
		/** simple constructor */
		Mapper();
		/** gets the long value associated to a string in the subjects map*/
		unsigned long addSubject(const std::string &s);
		/** gets the long value associated to a string in the predicates map*/
		unsigned long addPredicate(const std::string &p);
		/** gets the long value associated to a string in the objects map*/
		unsigned long addObject(const std::string &o);
		/** gets size of subjects, predicates and objects*/
		unsigned long getSize(const char &c) const;
		void save(const std::string &name);
};

#endif
