/**
\class parser

\brief provide parsing of turtle, ntriples and rdf files

This class is used for parsing general RDF file
*/
#ifndef __PARSER_H__
#define __PARSER_H__

#include "Model.h"
#include "TensorSPARQL.h"
#include "Mapper.h"
#include <raptor2.h>
#include <tuple>
#include <vector>
#include <string>

class Parser {
	private:
		std::string name_parser;
		std::string data_file;
		std::string file_uri;
	public:
		/** constructor that takes as parameter the uri of the file to parse, otherwise the file name */
		Parser(const std::string &uri);
		/** returns the name of the file used for parsing */
		std::string getFileName();
		/** initiates TensorSPARQL and Mapper objects */
		void initialize(TensorSPARQL &tensor, Mapper &maps);
};

#endif