#include "Model.h"
#include "Logger.h"
#include <vector>
#include <fstream>

Logger* Logger::logger = null;

int main(int argc, char* argv[]) {
	if(argc==3) {
		Logger::message("creazione del modello");
		std::stringstream convert;
		convert << argv[2];
		unsigned delta;
		convert >> delta;
		Model model(argv[1], delta);
		model.save();
		Logger::message("end");
		std::cin.get();
		return 0;
	}
}