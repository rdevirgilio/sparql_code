var searchData=
[
  ['add',['add',['../class_tensor_s_p_a_r_q_l.html#a0eddfb3b5af7cbb99b68a75a5bf346b0',1,'TensorSPARQL']]],
  ['addobject',['addObject',['../class_mapper.html#a0f9df206433f590e220d9aaff3d8b987',1,'Mapper']]],
  ['addpredicate',['addPredicate',['../class_mapper.html#a75979fd5612b8d2d10bab6374cbce74f',1,'Mapper']]],
  ['addselectvariable',['addSelectVariable',['../class_query_variables.html#af7571f308445acdf464be3d05fa042ee',1,'QueryVariables']]],
  ['addsubject',['addSubject',['../class_mapper.html#a45761d074ab70998aad659833d9d9ca7',1,'Mapper']]],
  ['addvariable',['addVariable',['../class_query_variables.html#aaa8d4b71e47af16149df709c12d3d275',1,'QueryVariables']]],
  ['all',['all',['../class_tensor_s_p_a_r_q_l.html#a9d7abf7bf60c18dc2ee8b09bd1c8c5f4',1,'TensorSPARQL']]]
];
