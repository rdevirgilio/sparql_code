#include "TensorPart.h"

void TensorPart::add(const Tuple &t) {
	NonZeroElements.push_back(t);
}

bool TensorPart::isPresent(const Triple &t) {
	for(unsigned i=0; i<NonZeroElements.size(); i++)
		if(NonZeroElements[i] == t)
			return true;
	return false;
}

std::vector<Tuple> TensorPart::getDelta(const Tuple &t) {
	std::vector<Tuple> res;
	for(unsigned i=0; i<NonZeroElements.size(); i++) {
		if(NonZeroElements[i][SUBJECT] == t[SUBJECT] || t[SUBJECT] == 0)
			if(NonZeroElements[i][PREDICATE] == t[PREDICATE] || t[PREDICATE] == 0)
				if(NonZeroElements[i][OBJECT] == t[OBJECT] || t[OBJECT] == 0)
					res.push_back(NonZeroElements[i]);
	}
	return res;
}

std::vector<Tuple> TensorPart::all() {
	return NonZeroElements;
}

void TensorPart::printAll(int rank) {
	std::vector<Tuple>::iterator it;
	for(it=NonZeroElements.begin(); it!=NonZeroElements.end(); it++) {
		std::cout << rank;
		(*it).print();
		std::cout << std::endl;
	}
	std::cout << std::endl;
}