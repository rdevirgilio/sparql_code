/**
\class Query

\brief Provide Query representation

This class represents a general query
*/
#ifndef __QUERY_H__
#define __QUERY_H__

#include <map>
#include <string>
#include "Model.h"
#include "Triple.h"
#include "QueryTriples.h"
#include "QueryVariables.h"
#include <set>

class Query {
	private:
		std::string query_text;
		std::string uri;
	public:
		/** constructor that wants a query string as input */
		Query(const std::string &query_string);
		/** executes the query */
		void execute(const Model &model, Result &res);
};

#endif
