#include "Model.h"
#include "Logger.h"
#include <fstream>
#include <sstream>

std::string getFileName(const std::string uri) {
	std::string data_file;
	int file_start = uri.find_last_of("/");
	if(file_start == std::string::npos) {
		data_file = uri;
	}
	int ext_point = data_file.find(".");
	if(ext_point!=std::string::npos)
		return data_file.substr(0,ext_point);
	else {
		Logger::message("file name error");
		return "";
	}
}

Model::Model(const std::string &file_uri) {
	std::string file_name = getFileName(file_uri); 
	std::ifstream file(file_name+"Maps.dat");
	if(!file.good()) {
		Logger::message(".dat files non presenti");
	}
	else {
		Logger::message(".dat files presenti");
		Logger::message("carica modello");
		load(file_name);
		int length = file_name.length();
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Bcast(&length, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		MPI_Bcast((char*)file_name.c_str(), length, MPI_CHAR, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
	}
}

void Model::load(const std::string &file_name) {
	unsigned long msizes[3];
	std::ifstream file;
	file.open(file_name+"Maps.dat", std::ios::binary);
	Logger::message("caricamento mappe");
	for(int i=1; i<4; i++) {
		char nmap = 0;
		unsigned msize;
		file.read((char*)&msize, sizeof(msize));
		msizes[i-1] = msize;
		while(msize!=0) {
			int length;
			file.read((char*)&length, sizeof(length));
			char* string = new char[length];
			file.read(string, length);
			std::string str(string);
			str = str.substr(0,length);
			unsigned long value;
			file.read((char*)&value, sizeof(value));
			switch (i) {
			case SUBJECT: mapper.setStringAndValue(SUBJECT,str,value); break;
			case PREDICATE: mapper.setStringAndValue(PREDICATE,str,value); break;
			case OBJECT: mapper.setStringAndValue(OBJECT,str,value); break;
			default: break;
			delete[] string;
			}
			msize--;
		}
	}
	tensor.set(msizes); 
	Logger::message("caricamento mappe completato");
}

std::vector<Tuple> Model::getAllTensorData()  const{
	return tensor.all();
}

std::vector<Tuple> Model::getDelta(const Delta &v) const{
	return tensor.getDelta(v);
}

std::vector<Tuple> Model::getDoubleDelta(const Delta &v1, const Delta &v2) const{
	return tensor.getDoubleDelta(v1, v2);
}

bool Model::presentInTensor(const Triple &t) const{
	return tensor.present(t);
}

std::string Model::getMapString(const char &role, const unsigned long &value) const{
	switch (role) {
	case SUBJECT: return mapper.getSubject(value);
	case PREDICATE: return mapper.getPredicate(value);
	case OBJECT: return mapper.getObject(value);
	}
}

unsigned long Model::findInMap(const char &role, const std::string &s) const{
	return mapper.finds(role, s);
}

Resolutions Model::conversion(char i, const Resolutions &res) const{
	Resolutions xs;
	xs.second = i;
	SolvedValues values;
	SolvedValues::const_iterator it;
	for(it=res.first.begin(); it!=res.first.end(); it++) {
		std::string s;
		switch (res.second) {
		case SUBJECT:
			s = getMapString(SUBJECT, *it); break;
		case PREDICATE:
			s = getMapString(PREDICATE, *it); break;
		case OBJECT:
			s = getMapString(OBJECT, *it); break;
		}
		unsigned long value = findInMap(i, s);
		if(value!=0)
			values.push_back(value);
	}
	xs.first = values;
	return xs;
}

void Model::showMap(char c) {
	string2value map = mapper.getMap(c);
	string2value::iterator it;
	for(it=map.begin(); it!=map.end(); it++) {
		std::stringstream ss;
		ss << it->second;
		Logger::message(it->first+" "+ss.str());
	}
}

void Model::printTensor() {
	tensor.printAll();
}
