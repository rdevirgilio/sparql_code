/**
\class QueryVariables

\brief Provide Query variables management

This class is responsable for variables in the query, it handles map for convertion
between their string values and their long negative representation in the model.
*/
#ifndef __QUERYVARIABLES_H__
#define __QUERYVARIABLES_H__

#include <string>
#include <vector>
#include <set>
#include <map>
#include "Triple.h"

typedef std::vector<unsigned long> SolvedValues;
typedef std::pair<SolvedValues, char> Resolutions;
typedef std::map<std::string, Resolutions> Result;

class QueryVariables {
	private:
		int val;
		std::vector<long> selectVariables;
		std::map<std::string, long> string2index;
		std::map<int, Resolutions> variable2values;
		std::string getString(const long &i);
	public:
		/** simple constructor */
		QueryVariables();
		/** adds a new variable and returns the index associated with it*/
		long addVariable(const std::string x);
		/** adds select variable*/
		void addSelectVariable(const std::string x);
		/** updates the map with new solved values*/
		void updateSolvedVariable(const long &var, const Resolutions &values);
		/** gets the long value associated to a variable string */
		long getVariable(const std::string &x);
		/** given the long value of a variable as input, it returns the solved values linked to it */
		Resolutions getSolvedVariable(const long &i);
		/** it returns a map of the select variables and their solved values*/
		void getResult(Result &res);
		/** this function returns the variable index in the Triple t*/
		char roleVariable(const Triple t);
		/** this method returns the constant index in the Triple t given as input*/
		char roleCostant(const Triple t);
		/** returns the value of DOF of a Triple t, considering also solved variables*/
		char DOF(Triple t);
		/** shows content of QueryVariables */
		void show();
};

#endif
