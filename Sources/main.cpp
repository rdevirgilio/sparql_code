#include "Model.h"
#include "Query.h"
#include "Logger.h"
#include "mpi.h"
#include "TensorPart.h"
#include <rasqal.h>
#include <raptor2.h>
#include <vector>
#include <fstream>
Logger* Logger::logger = null;

int main(int argc, char* argv[]) {
	int numtasks, rank, rc;
	rc = MPI_Init(&argc, &argv);
	if(rc != MPI_SUCCESS) {
		Logger::message("error in starting MPI");
		MPI_Abort(MPI_COMM_WORLD, rc);
	}
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if(argc==3) {
		if(rank==0) {
			Logger::message("creazione del modello");
			Model model(argv[1]);
			std::string query_text = argv[2];
/*			model.showMap(SUBJECT);
			model.showMap(PREDICATE);
			model.showMap(OBJECT); */
			Query query(query_text);
			Result result;
			query.execute(model, result);
			Logger::message("Il risultato dell'interrogazione e': ");
			for(Result::iterator it=result.begin(); it!=result.end(); it++) {
				Logger::message((it->first)+": ");
				char domain = it->second.second;
				char c = 0;
				for(SolvedValues::iterator it1=it->second.first.begin(); it1!=it->second.first.end() && c<10; it1++) {
					c++;
					Logger::message(model.getMapString(domain,*it1));
				}
				std::stringstream ss;
				ss << it->second.first.size() << " elementi";
				Logger::message("di "+ss.str());
			} 
		}
		else {
			int tag = rank;
			TensorPart part;
			unsigned long tuple[4];
			int length;
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Bcast(&length, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
			char* filename = new char[length];
			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Bcast(filename, length, MPI_CHAR, 0, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD); 
			std::stringstream srank;
			srank << rank;
			std::string name(filename);
			delete[] filename;
			name = name.substr(0,length);
			std::ifstream file;
			file.open(name+"part"+srank.str()+".dat", std::ios::binary);
			unsigned size;
			file.read((char*)&size, sizeof(size));
			for(unsigned i=0; i<size; i++) {
				Tuple t;
				unsigned long subject;
				unsigned long predicate;
				unsigned long object;
				file.read((char*)&subject, sizeof(subject));
				file.read((char*)&predicate, sizeof(predicate));
				file.read((char*)&object, sizeof(object));
				t(subject, predicate, object);
				part.add(t);
			} 
			file.close();
//			part.printAll(rank);
			while (true) {
				MPI_Barrier(MPI_COMM_WORLD);
				MPI_Bcast(&tuple, 4, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
				MPI_Barrier(MPI_COMM_WORLD);
				std::vector<Tuple> result;
				Tuple t(tuple);
				Triple tr;
				int partSize;
				int opt = tuple[3];
				int res;
				unsigned long* returned_tuples;
				std::stringstream ssd;
				std::stringstream sres;
				ssd << opt;
				switch (opt) {
				case 1:
					tr.setValue(SUBJECT, t[SUBJECT]);
					tr.setValue(PREDICATE, t[PREDICATE]);
					tr.setValue(OBJECT, t[OBJECT]);
					res = part.isPresent(tr);
					MPI_Barrier(MPI_COMM_WORLD);
					MPI_Gather(&res, 1, MPI_INT, null, null, MPI_INT, 0, MPI_COMM_WORLD);
					MPI_Barrier(MPI_COMM_WORLD);
					break;
				case 2:
				case 3:
					unsigned long returned_tuple[3];
					if(opt == 2)
						result = part.getDelta(t);
					if(opt == 3)
						result = part.all();
					partSize = result.size();
					MPI_Barrier(MPI_COMM_WORLD);
					MPI_Gatherv(&partSize, 1, MPI_INT, null, null, null, MPI_INT, 0, MPI_COMM_WORLD);
					MPI_Barrier(MPI_COMM_WORLD);
					returned_tuples = new unsigned long[3*partSize];
					for(int i=0; i<partSize; i++) {
						result[i].getArray(returned_tuple);
						returned_tuples[3*i] = returned_tuple[0];
						returned_tuples[3*i+1] = returned_tuple[1];
						returned_tuples[3*i+2] = returned_tuple[2];
					}
					MPI_Barrier(MPI_COMM_WORLD);
					MPI_Gatherv(returned_tuples, 3*partSize, MPI_UNSIGNED_LONG, null, null, null, MPI_INT, 0, MPI_COMM_WORLD);
					MPI_Barrier(MPI_COMM_WORLD);
					delete[] returned_tuples;
					break;
				default:
					Logger::message("caso non gestito");
				}
			} 
		} 
	} 
	Logger::message("end");
	std::cin.get();
	MPI_Finalize();
	return 0;
}