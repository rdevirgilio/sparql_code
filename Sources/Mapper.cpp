#include "Mapper.h"
#include "Triple.h"
#include "Logger.h"
#include <map>
#include <list>
#include <string>
#include <iostream>

Mapper::Mapper() {
	i = j = k = 1;
}

string2value Mapper::getMap(const char &role) {
	switch (role) {
	case SUBJECT:
		return subjects;
	case PREDICATE:
		return predicates;
	case OBJECT:
		return objects;
	default: 
		Logger::message("Mapper->getMap: caso non gestito");
		string2value no_value;
		return no_value;
	}
}
 
void Mapper::setStringAndValue(const char &c, const std::string &s, const unsigned long &l) {
	switch (c) {
	case SUBJECT: i++; subjects[s] = l; break;
	case PREDICATE: j++; predicates[s] = l; break;
	case OBJECT: k++; objects[s] = l; break;
	default: Logger::message("errore valore non valido nel settaggio delle mappe");
	}
}

unsigned long Mapper::addSubject(const std::string &s) {
	unsigned long val = subjects[s];
	if(val == 0) {
		val = i;
		subjects[s] = i;
		i++;
	}
	return val;
}

unsigned long Mapper::addPredicate(const std::string &s) {
	unsigned long val = predicates[s];
	if(val == 0) {
		val = j;
		predicates[s] = j;
		j++;
	}
	return val;
}

unsigned long Mapper::addObject(const std::string &s) {
	unsigned long val = objects[s];
	if(val == 0) {
		val = k;
		objects[s] = k;
		k++;
	}
	return val;
}

unsigned long Mapper::getSize(const char &c) const{
	switch (c) {
	case SUBJECT: return subjects.size();
	case PREDICATE: return predicates.size();
	case OBJECT: return objects.size();
	default: Logger::message("errore nel ritorno delle dimensioni di Mapper"); return 0;
	}
}

std::string Mapper::getSubject(const unsigned long &value) const{
	if(value>i) {
		Logger::message("errore valore eccede il massimo valore per SubjectsMap");
		std::cout << value << " valore ritornato" << std::endl; 
		return "";		
	}
	else {
		string2value::const_iterator it;
		for(it=subjects.begin(); it!=subjects.end(); it++) 
			if(it->second==value)
				return it->first;
	}
	Logger::message("stringa non trovata in SubjectsMap");
	return "";
}

std::string Mapper::getPredicate(const unsigned long &value) const{
	if(value>j) {
		Logger::message("errore valore eccede il massimo valore per PredicatesMap");
		return "";		
	}
	else {
		string2value::const_iterator it;
		for(it=predicates.begin(); it!=predicates.end(); it++) 
			if(it->second==value)
				return it->first;
	}
	Logger::message("stringa non trovata in PredicatesMap");
	return "";
}

std::string Mapper::getObject(const unsigned long &value) const{
	if(value>k) {
		Logger::message("errore valore eccede il massimo valore per ObjectsMap");
		return "";
	}
	else {
		string2value::const_iterator it;
		for(it=objects.begin(); it!=objects.end(); it++) 
			if(it->second==value)
				return it->first;
	}
	Logger::message("stringa non trovata in ObjectsMap");
	return "";
}

unsigned long Mapper::finds(char c, const std::string &s) const{
	string2value::const_iterator it;
	switch (c) {
	case SUBJECT:
		it = subjects.find(s);
		if(it!=subjects.end())
			return it->second;
		else Logger::message("valore non trovato in SubjectsMap");
		return 0;
	case PREDICATE:
		it = predicates.find(s); 
		if(it!=predicates.end())
			return it->second;
		else Logger::message("valore non trovato in PredicatesMap");
		return 0;
	case OBJECT:
		it = objects.find(s);
		if(it!=objects.end()) 
			return it->second;
		else Logger::message("valore non trovato in ObjectsMap");
		return 0;
	default: Logger::message("valore non valido per la ricerca");
		return 0;
	}
}