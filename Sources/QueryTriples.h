/**
\class QueryTriples

\brief Provide triple patterns management

This class holds triple patterns of a query and handle them as a priority queue
with the highest DOF value pattern on the top of it
*/
#ifndef __QueryTriples_H__
#define __QueryTriples_H__

#include <list>
#include "QueryVariables.h"
#include "Triple.h"

class QueryTriples {
	private:
		QueryVariables * vars;
		std::list<Triple>::iterator ONE, MINUSONE, MINUSTHREE;
		std::list<Triple> triples;
		std::list<Triple> processedPatterns;
		/** adds a Triple t to the queue*/
		void add(Triple t, short setting = 1);
		/** returns true only if the queue is arranged*/
		bool arranged(); 
	public:
		/** constructor which initializes iterators' values*/
		QueryTriples();
		/** sets the vector of Triple to list of Triple, since we need to navigate back and forward */
		void setData(std::vector<Triple> data);
		/** sets the vars pointer to point to v in order to get the function DOF, used to order the queue*/
		void setVars(QueryVariables * v);
		/** tests if the queue is empty or not*/
		bool isEmpty();
		/** gets the element on the top and pops it*/
		Triple getPattern();
		/** gets the size of the queue*/
		unsigned getSize();
		/** updates the queue at end of every query iteration in order to keep the queue ordered*/ 
		void update();
		/** gets applied patterns */
		std::list<Triple> getProcessedPatterns();
};

#endif
