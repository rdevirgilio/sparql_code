#ifndef __QUERYFILTERS_H__
#define __QUERYFILTERS_H__

#include "QueryVariables.h"
#include "Model.h"
#include <vector>
#include <string>

enum filter_opt {
	UNKNOWN,
	AND,
	OR,
	EQUAL,
	NEQUAL,
	LESS_THAN,
	GREATER_THAN,
	LESS_EQUAL,
	GREATER_EQUAL,
	UMINUS,
	PLUS,
	MINUS,
	STAR,
	SLASH,
	REM,
	STR_EQUAL,
	STR_NEQUAL,
	STR_MATCH,
	STR_NMATCH,
	TILDE,
	BANG,
	LITERAL,
	FUNCTION,
	BOUND,
	STR,
	LANG,
	DATATYPE,
	IS_URI,
	IS_BLANK,
	IS_LITERAL,
	CAST,
	ORDER_ASC,
	ORDER_DESC,
	LANG_MATCHES,
	REGEX,
	GROUP_ASC,
	GROUP_DESC,
	COUNT,
	VAR_STAR,
	SAME_TERM,
	SUM,
	AVG,
	MIN,
	MAX,
	COALESCE,
	IF,
	URI,
	IRI,
	STR_LANG,
	STR_DT,
	BNODE,
	GROUP_CONCAT,
	SAMPLE,
	IN,
	NOT_IN,
	IS_NUMERIC,
	YEAR,
	MONTH,
	DAY,
	HOURS,
	MINUTES,
	SECONDS,
	TIMEZONE,
	CURRENT_DATETIME,
	NOW,
	FROM_UNIXTIME,
	TO_UNIXTIME,
	CONCAT,
	STR_LEN,
	SUB_STR,
	UCASE,
	LCASE,
	STR_STARTS,
	STRENDS,
	CONTAINS,
	ENCODE_FOR_URI,
	TZ,
	RAND,
	ABS,
	ROUND,
	CEIL,
	FLOOR,
	MD5,
	SHA1,
	SHA224,
	SHA256,
	SHA384,
	SHA512,
	STR_BEFORE,
	STR_AFTER,
	REPLACE,
	UUID,
	STRUUID
};

enum type { VAR, LIT };

enum numeric_convertion_type { UNDEFINED, INTEGER, FLOAT, DOUBLE, LONG, UINTEGER, ULONG };

struct Term {
	std::string name;
	type type;
};

struct ConvertedValue {
	numeric_convertion_type type;
	int int_value;
	float float_value;
	double double_value;
	long long_value;
	unsigned u_value;
	unsigned long ul_value;
};

typedef std::vector<Term> FilterArgs;
typedef std::pair<Resolutions, ConvertedValue> Solutions;
typedef std::pair<long, Solutions> TempResolutions;

struct Filter {
	std::string opt;
	FilterArgs vars;
	FilterArgs lits;
	std::vector<Filter> composed_terms;
};

class QueryFilters {
	private:
		std::map<std::string, filter_opt> string2opt;
		std::vector<Filter> filters;
		/** applies a filter */
		TempResolutions applyFilter(const Filter &f, QueryVariables &var, const Model &model);
	public:
		QueryFilters();
		/** adds a filter to QueryFilters */
		void add(const Filter &f);
		/** apply all filters in Queryfilters */
		void apply(QueryVariables &vars, const Model &model);
};

#endif