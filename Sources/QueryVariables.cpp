#include "QueryVariables.h"
#include "Triple.h"
#include "Logger.h"
#include <vector>
#include <map>
#include <iostream>

std::string QueryVariables::getString(const long &i) {
	std::map<std::string, long>::iterator it;
	for(it=string2index.begin(); it!=string2index.end(); it++)
		if(it->second==i)
			return it->first;
}

QueryVariables::QueryVariables() {
	val = -1;
}

long QueryVariables::addVariable(std::string x) {
	std::map<std::string, long>::iterator it;
	it = string2index.find(x);
	if(it==string2index.end()) {
		string2index[x] = val;
		variable2values[val];
		val--;
		return string2index[x];
	}
	return it->second;
}

void QueryVariables::addSelectVariable(std::string x) {
	selectVariables.push_back(string2index[x]);
}

char QueryVariables::DOF(Triple t) {
	char dof = t.getDegreeOfFreedom();
	for(int i=1; i<4; i++) {
		long value;
		switch (i) {
		case SUBJECT:
			value = t[SUBJECT]; break;
		case PREDICATE:
			value = t[PREDICATE]; break;
		case OBJECT:
			value = t[OBJECT]; break;
		}
		if(value<0)
			if(getSolvedVariable(value).first.size()!=0)
				switch (dof) {
				case -3:
					dof = -1; break;
				case -1:
					dof = 1; break;
				case 1:
					dof = 3; break;
				}
	}
	return dof;
}

char QueryVariables::roleVariable(const Triple t) {
	for(int i=1; i<4; i++) {
		long value;
		switch (i) {
		case SUBJECT:
			value = t[SUBJECT]; break;
		case PREDICATE:
			value = t[PREDICATE]; break;
		case OBJECT:
			value = t[OBJECT]; break;
		}
		if(value<0)
			if(variable2values.at(value).first.size()==0)
				switch (i) {
				case SUBJECT:
					return SUBJECT;
				case PREDICATE:
					return PREDICATE;
				case OBJECT:
					return OBJECT;
				}
	}
}

char QueryVariables::roleCostant(const Triple t) {
	for(int i=1; i<4; i++) {
		long value;
		switch (i) {
		case SUBJECT:
			value = t[SUBJECT]; break;
		case PREDICATE:
			value = t[PREDICATE]; break;
		case OBJECT:
			value = t[OBJECT]; break;
		}
		if(value>=0 || variable2values.at(value).first.size()!=0)
			switch (i) {
			case SUBJECT:
				return SUBJECT;
			case PREDICATE:
				return PREDICATE;
			case OBJECT:
				return OBJECT;
			}
	}
}

void QueryVariables::updateSolvedVariable(const long &var, const Resolutions &values) {
	variable2values[var] = values;
}

long QueryVariables::getVariable(const std::string &x) {
	Logger::message(x);
	std::map<std::string, long>::iterator it = string2index.find(x);
	if(it != string2index.end())
		return it->second;
	Logger::message("variable not present error");
	return 0;
}

Resolutions QueryVariables::getSolvedVariable(const long &i) {
	std::map<int, Resolutions>::iterator it = variable2values.find(i);
	if(it!=variable2values.end())
		return variable2values[i];
	Logger::message("soluzioni non trovate");
	Resolutions vuoto;
	vuoto.second = 0;
	return vuoto;
}

void QueryVariables::getResult(Result &res) {
	std::vector<long>::iterator it;
	std::string var;
	for(it=selectVariables.begin(); it!= selectVariables.end(); it++) {
		var = getString(*it);
		res[var] = variable2values[*it];
	}
}

void QueryVariables::show() {
	std::map<int, Resolutions>::iterator it;
	SolvedValues::iterator it2;
	for(it=variable2values.begin(); it!= variable2values.end(); it++) {
		std::cout << it->first << ": ";
		for(it2=it->second.first.begin(); it2!=it->second.first.end(); it2++)
			std::cout << *it2 << " ";
		std::cout << std::endl;
	}
	std::cout << std::endl;
}