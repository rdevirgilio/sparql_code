/**
\class TensorSPARQL

\brief Provide Tensor representation

This class realizes the Tensor representation for containing data.
*/
#ifndef __TENSORSPARQL_H__
#define __TENSORSPARQL_H__

#include <vector>
#include "Tuple.h"
#include "mpi.h"

typedef std::pair<unsigned char, unsigned long> Delta;

class TensorSPARQL {
	private:
		unsigned long * dimensions;
		int process;
		int numtasks;
		std::vector<Tuple> getElements(Tuple &t, unsigned long opt) const;
	public:
		TensorSPARQL();
        /** this method adds dimensions to this Tensor */
		void set(unsigned long * dimensions);
		/** tests if a Triple element is present or not */
		bool present(const Triple &element) const;
		std::vector<Tuple> getDoubleDelta(const Delta &d1, const Delta &d2) const;
		/** realizes the Tensor product with a Kronecker delta */
		std::vector<Tuple> getDelta(const Delta &delta) const;
		/** returns all NonZeroElement of the Tensor */
		std::vector<Tuple> all() const;
		/** prints tensor content */
		void printAll();
};

#endif
