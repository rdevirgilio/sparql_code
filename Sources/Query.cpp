#include "Query.h"
#include "ConjunctiveQuery.h"
#include "Logger.h"
#include <algorithm>
#include <raptor2.h>
#include <rasqal.h>
#include <vector>
#include <fstream>

Query::Query(const std::string &q) {
	std::fstream file(q, std::fstream::in);
	if(file.good()) {
		std::getline(file, query_text, '\0');
		file.close();
		Logger::message("creazione della query");
		Logger::message(query_text);
	}
	else Logger::message("query file non valido");
}

Filter getFilter(rasqal_expression* filter) {
	Filter flt;
	FilterArgs vars;
	FilterArgs lits;
	std::vector<Filter> flts;
	rasqal_op filter_opt = filter->op;
	for(int i=0; i<4; i++) {
		rasqal_expression* arg;
		switch (i) {
		case 0: arg = filter->arg1; break;
		case 1: arg = filter->arg2; break;
		case 2: arg = filter->arg3; break;
		case 3: arg = filter->arg4; break;
		default: break;
		}
		if(arg) {
			rasqal_literal* element = arg->literal;
			if(element) {
				std::string label;
				Term term;
				if(element->type == RASQAL_LITERAL_VARIABLE) {
					rasqal_variable* var = rasqal_literal_as_variable(element);
					label = (const char*)var->name;
					Logger::message("nome della variabile "+label);
					term.type = VAR;
					term.name = label;
					vars.push_back(term);
				}
				else {
					label = (const char*)rasqal_literal_as_string(element);
					Logger::message("nome del letterale "+label);
					term.type = LIT;
					term.name = label;
					lits.push_back(term);
				}
			}
			else {
				flts.push_back(getFilter(arg));
			}
		}
	}
	flt.opt = rasqal_expression_op_label(filter_opt);
	flt.vars = vars;
	flt.lits = lits;
	flt.composed_terms = flts;
	return flt;
}

void getFilters(rasqal_query* query, QueryFilters &filters) {
	Filter flt;
	raptor_sequence* graph_patterns = rasqal_query_get_graph_pattern_sequence(query);
	int size  = raptor_sequence_size(graph_patterns);
	std::stringstream ss;
	ss << "numero di gp: " << size;
	Logger::message(ss.str());
	for(int i=0; i<size; i++) {
		rasqal_graph_pattern* gp = rasqal_query_get_graph_pattern(query, i);
		rasqal_graph_pattern_operator opt = rasqal_graph_pattern_get_operator(gp);
		if(opt==RASQAL_GRAPH_PATTERN_OPERATOR_FILTER){
			FilterArgs arguments;
			rasqal_expression* filter = rasqal_graph_pattern_get_filter_expression(gp);
			Filter flt = getFilter(filter);
			filters.add(flt);
		}
	}
}

raptor_sequence* addGPTripleToSequence(raptor_sequence* s, rasqal_graph_pattern* gp) {
	int i = 0;
	while(rasqal_graph_pattern_get_triple(gp, i)!=NULL) {
		rasqal_triple* t = rasqal_graph_pattern_get_triple(gp, i);
		raptor_sequence_push(s, t);
		i++;
	}
	return s;
}

void raptor_sequence_to_vector(raptor_sequence* triples, std::vector<Triple> &qtriples, const Model &model, QueryVariables &vars) {
	Triple t;
	for(int i=0; i<raptor_sequence_size(triples); i++) {
		rasqal_triple* triple = (rasqal_triple*)raptor_sequence_get_at(triples, i);
		rasqal_literal* subject = triple->subject;
		if(rasqal_literal_value(subject) == NULL) 
			t.setValue(SUBJECT, vars.addVariable((const char*)rasqal_literal_as_variable(subject)->name));
		else t.setValue(SUBJECT, model.findInMap(SUBJECT, (const char*)rasqal_literal_as_string(subject)));
		rasqal_literal* predicate = triple->predicate;
		if(rasqal_literal_value(predicate) == NULL) 
			t.setValue(PREDICATE, vars.addVariable((const char*)rasqal_literal_as_variable(predicate)->name));
		else t.setValue(PREDICATE, model.findInMap(PREDICATE, (const char*)rasqal_literal_as_string(predicate)));
		rasqal_literal* object = triple->object;
		if(rasqal_literal_value(object) == NULL) 
			t.setValue(OBJECT, vars.addVariable((const char*)rasqal_literal_as_variable(object)->name));
		else t.setValue(OBJECT, model.findInMap(OBJECT, (const char*)rasqal_literal_as_string(object)));
		qtriples.push_back(t);
	}
}

SolvedValues merge(SolvedValues &a, SolvedValues &b) {
	SolvedValues output;
	std::sort(a.begin(), a.end());
	std::sort(b.begin(), b.end());
	set_union(a.begin(), a.end(), b.begin(), b.end(), inserter(output, output.begin()));
	return output; 
}

void merge_results(Result &res1, Result &res2, const Model &model) {
	Result::iterator it;
	for(it=res2.begin(); it!=res2.end(); it++) {
		Resolutions res = res1[it->first];
		if(res.first.size() == 0)
			res1[it->first] = it->second;
		else if(res.second == it->second.second)
			res1[it->first].first = merge(res.first, it->second.first);
		else res1[it->first].first = merge(res.first, model.conversion(res.second, it->second).first);
	}
}

void Query::execute(const Model &model, Result &res) {
	QueryVariables vars;
	QueryTriples patterns;
	QueryFilters filters;
	std::vector<Triple> qtriples;
	Logger::message("inizio parsing della query");
	rasqal_world* world = rasqal_new_world();
	raptor_world* world1 = raptor_new_world();
	rasqal_query* query = rasqal_new_query(world, "sparql", NULL);
	const char* query_string = query_text.c_str();
	rasqal_query_prepare(query, (const unsigned char*)query_string, NULL);
	Logger::message("parsing di variabili");
	raptor_sequence* variables = rasqal_query_get_bound_variable_sequence(query);
	for(int i=0; i<raptor_sequence_size(variables); i++) {
		rasqal_variable* variable = (rasqal_variable*) raptor_sequence_get_at(variables, i);
		vars.addVariable((const char*)(variable->name));
		vars.addSelectVariable((const char*)(variable->name));
	}
	QueryVariables union_vars = vars;
	QueryVariables optional_vars = vars;
	Logger::message("parsing di triple pattern");
	raptor_sequence* union_triples = raptor_new_sequence(NULL, NULL);
	raptor_sequence* union_gps = raptor_new_sequence(NULL, NULL);
	raptor_sequence* optional_triples = raptor_new_sequence(NULL, NULL);
	raptor_sequence* optional_gps = raptor_new_sequence(NULL, NULL);
	raptor_sequence* triples = raptor_new_sequence(NULL, NULL); 
	raptor_sequence* graph_patterns = rasqal_query_get_graph_pattern_sequence(query);
	int size = raptor_sequence_size(graph_patterns);
	std::stringstream ss;
	ss << size;
	Logger::message("numero di graph pattern e' di "+ ss.str());
	if(size<0)
		triples = rasqal_query_get_triple_sequence(query);
	rasqal_graph_pattern* gpi;
	for(int i=0; i<size; i++) {
		int j = 0;
		rasqal_graph_pattern* gp = (rasqal_graph_pattern*) raptor_sequence_get_at(graph_patterns, i);
		rasqal_graph_pattern_operator opt = rasqal_graph_pattern_get_operator(gp);
		switch (opt) {
		case RASQAL_GRAPH_PATTERN_OPERATOR_UNION:
			Logger::message("caso UNION");
			union_gps = rasqal_graph_pattern_get_sub_graph_pattern_sequence(gp);
			gpi = (rasqal_graph_pattern*) raptor_sequence_get_at(union_gps, j);
			while(gpi != NULL) {
				union_triples = addGPTripleToSequence(union_triples, gpi);
				j++;
				gpi = (rasqal_graph_pattern*) raptor_sequence_get_at(union_gps, j); 
			} break;
		case RASQAL_GRAPH_PATTERN_OPERATOR_OPTIONAL:
			Logger::message("caso OPTIONAL");
			optional_gps = rasqal_graph_pattern_get_sub_graph_pattern_sequence(gp);
			gpi = (rasqal_graph_pattern*) raptor_sequence_get_at(optional_gps, j);
			while(gpi != NULL) {
				optional_triples = addGPTripleToSequence(optional_triples, gpi);
				j++;
				gpi = (rasqal_graph_pattern*) raptor_sequence_get_at(optional_gps, j);
			} break;
		case RASQAL_GRAPH_PATTERN_OPERATOR_BASIC:
			Logger::message("caso BASIC");
			triples = addGPTripleToSequence(triples, gp); break;
		default:
			Logger::message("caso non gestito");
		}
	}
	raptor_sequence_to_vector(triples, qtriples, model, vars);
	Logger::message("parsing di filters");
	getFilters(query, filters);
	patterns.setData(qtriples);
	patterns.setVars(&vars);
	ConjunctiveQuery q(patterns, vars, filters);
	Logger::message("fine parsing della query");
	q.execute(model, res);
	if(raptor_sequence_size(union_triples)>0) {
		Logger::message("esecuzione parte union");
		Result union_res;
		QueryTriples union_patterns;
		std::vector<Triple> union_qtriples;
		raptor_sequence_to_vector(union_triples, union_qtriples, model, union_vars);
		union_patterns.setData(union_qtriples);
		union_patterns.setVars(&union_vars);
		ConjunctiveQuery union_query(union_patterns, union_vars, filters);
		union_query.execute(model, union_res);
		merge_results(res, union_res, model);
	}
	if(raptor_sequence_size(optional_triples)>0) {
		Logger::message("esecuzione parte optional");
		Result optional_res;
		std::vector<Triple>::iterator it;
		std::vector<Triple> optional_qtriples;
		std::vector<Triple> optional_triple_patterns;
		QueryTriples optional_patterns;
		raptor_sequence_to_vector(optional_triples, optional_qtriples, model, optional_vars);
		for(it=optional_qtriples.begin(); it!=optional_qtriples.end(); it++) {
			optional_triple_patterns = qtriples;
			optional_triple_patterns.push_back(*it);
			QueryVariables ovars = optional_vars;
			optional_patterns.setData(optional_triple_patterns);
			optional_patterns.setVars(&ovars);
			ConjunctiveQuery optional_query(optional_patterns, optional_vars, filters);
			optional_query.execute(model, optional_res);
			merge_results(res, optional_res, model);
		}
	}
}