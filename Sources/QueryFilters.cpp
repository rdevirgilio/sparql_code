#include "QueryFilters.h"
#include "Logger.h"
#include <algorithm>
#include <vector>

ConvertedValue convert(const std::string &str) {
	std::stringstream ss;
	std::string value;
	std::string s;
	std::string convertion_type;
	ConvertedValue converted_value;
	if(str.at(0) == '\"')
		s = str.substr(1, str.length()-2);
	else s = str;
	int pos;
	int end;
	pos = s.find("^");
	if(pos == std::string::npos) {
		value = s;
		ss << value;
		converted_value.type = INTEGER;
		ss >> converted_value.int_value;
	}
	else {
		end = s.find_first_of("^");
		value = s.substr(0, end);
		end = s.find_last_of("#");
		convertion_type = s.substr(end+1, s.length()-1);
		ss << value;
		if(convertion_type == "float") {
			ss >> converted_value.float_value;
			converted_value.type = FLOAT;
		}
		else if(convertion_type == "double") { 
			ss >> converted_value.double_value;
			converted_value.type = DOUBLE;
		}
		else if(convertion_type == "long") {
			ss >> converted_value.long_value;
			converted_value.type = LONG;
		}
		else if(convertion_type == "integer" || convertion_type == "short" || convertion_type == "int") {
			ss >> converted_value.int_value;
			converted_value.type = INTEGER;
		}
		else if(convertion_type == "unsignedLong") {
			ss >> converted_value.ul_value;
			converted_value.type = ULONG;
		}
		else if(convertion_type == "unsignedShort" || convertion_type == "unsignedInt" || convertion_type == "positiveInteger" || convertion_type == "nonNegativeInteger") {
			ss >> converted_value.u_value;
			converted_value.type = UINTEGER;
		}
		else Logger::message("QueryFilters->convert:: unknown type to convert");
	}
	return converted_value;
}

bool operator<(const ConvertedValue &c1, const ConvertedValue &c2) {
	switch (c1.type) {
	case INTEGER:
		return c1.int_value < c2.int_value;
	case FLOAT:
		return c1.float_value < c2.float_value;
	case DOUBLE:
		return c1.double_value < c2.double_value;
	case LONG:
		return c1.long_value < c2.long_value;
	case UINTEGER:
		return c1.u_value < c2.u_value;
	case ULONG:
		return c1.ul_value < c2.ul_value;
	default:
		Logger::message("type not handled");
		return false;
	}
}

bool operator>(const ConvertedValue &c1, const ConvertedValue &c2) {
	switch (c1.type) {
	case INTEGER:
		return c1.int_value > c2.int_value;
	case FLOAT:
		return c1.float_value > c2.float_value;
	case DOUBLE:
		return c1.double_value > c2.double_value;
	case LONG:
		return c1.long_value > c2.long_value;
	case UINTEGER:
		return c1.u_value > c2.u_value;
	case ULONG:
		return c1.ul_value > c2.ul_value;
	default:
		Logger::message("type not handled");
		return false;
	}
}

bool operator<=(const ConvertedValue &c1, const ConvertedValue &c2) {
	switch (c1.type) {
	case INTEGER:
		return c1.int_value <= c2.int_value;
	case FLOAT:
		return c1.float_value <= c2.float_value;
	case DOUBLE:
		return c1.double_value <= c2.double_value;
	case LONG:
		return c1.long_value <= c2.long_value;
	case UINTEGER:
		return c1.u_value <= c2.u_value;
	case ULONG:
		return c1.ul_value <= c2.ul_value;
	default:
		Logger::message("type not handled");
		return false;
	}
}

bool operator>=(const ConvertedValue &c1, const ConvertedValue &c2) {
	switch (c1.type) {
	case INTEGER:
		return c1.int_value >= c2.int_value;
	case FLOAT:
		return c1.float_value >= c2.float_value;
	case DOUBLE:
		return c1.double_value >= c2.double_value;
	case LONG:
		return c1.long_value >= c2.long_value;
	case UINTEGER:
		return c1.u_value >= c2.u_value;
	case ULONG:
		return c1.ul_value >= c2.ul_value;
	default:
		Logger::message("type not handled");
		return false;
	}
}

ConvertedValue operator/(ConvertedValue &c1, const int &i) {
	switch (c1.type) {
	case INTEGER:
		c1.int_value = c1.int_value/i;
	case FLOAT:
		c1.float_value = c1.float_value/i;
	case DOUBLE:
		c1.double_value = c1.double_value/i;
	case LONG:
		c1.long_value = c1.long_value/i;
	case UINTEGER:
		c1.u_value = c1.u_value/i;
	case ULONG:
		c1.ul_value = c1.ul_value/i;
	default:
		Logger::message("QueryFilters->operator/: invalid type");
	}
	return c1;
}

ConvertedValue operator+(const ConvertedValue &c1, const ConvertedValue &c2) {
	ConvertedValue res;
	res.type = c1.type;
	res.double_value = c1.double_value + c2.double_value;
	res.float_value = c1.float_value + c2.float_value;
	res.int_value = c1.int_value + c2.int_value;
	res.long_value = c1.long_value + c2.long_value;
	res.ul_value = c1.ul_value + c2.ul_value;
	res.u_value = c1.u_value + c2.u_value;
	return res;
}

TempResolutions QueryFilters::applyFilter(const Filter &f, QueryVariables &vars, const Model &model) {
	std::vector<unsigned long>::iterator it;
	std::vector<ConvertedValue> numeric_values;
	Resolutions updatedTemp;
	Resolutions tempResult;
	Solutions sols;
	SolvedValues passedValue;
	std::stringstream ss;
	filter_opt op = string2opt[f.opt];
	TempResolutions t1;
	TempResolutions t2;
	ConvertedValue compare_value;
	ConvertedValue numeric_value;
	unsigned long value;
	ConvertedValue c_value, sum, max, min;
	max.type = min.type = UNDEFINED;
	int occ = 0;
	char role;
	long variable;
	variable = vars.getVariable((f.vars.front()).name);
	tempResult = vars.getSolvedVariable(variable);
	role = tempResult.second;
	updatedTemp.second = role;
	switch (op) {
	case UNKNOWN: Logger::message("operazione sconosciuta"); break;
	case AND:
		t1 = applyFilter(f.composed_terms[0], vars, model);
		t2 = applyFilter(f.composed_terms[1], vars, model);
		if(t1.first!=0 && t2.first!=0) {
			vars.updateSolvedVariable(t1.first, t1.second.first);
			vars.updateSolvedVariable(t2.first, t2.second.first);
		}
		break;
	case OR:
		t1 = applyFilter(f.composed_terms[0], vars, model);
		t2 = applyFilter(f.composed_terms[1], vars, model);
		vars.updateSolvedVariable(t1.first, t1.second.first);
		vars.updateSolvedVariable(t2.first, t2.second.first);
		break;
	case LESS_THAN:
	case GREATER_THAN:
	case LESS_EQUAL:
	case GREATER_EQUAL:
		compare_value = convert(f.lits.front().name);
	case EQUAL:
	case NEQUAL:	
		value = model.findInMap(role, (f.lits.front()).name);
		if(value != 0) {
			ss << "il valore per il literal e' " << value;
			Logger::message(ss.str());
			ss.str("");
		}
		else Logger::message("literal non trovato in map");
		for(it=tempResult.first.begin(); it!=tempResult.first.end(); it++) {
			ss << *it;
//			Logger::message("processando il valore "+ss.str());
			ss.str("");
			bool pass = false;
			switch (op) {
			case EQUAL:
				if((*it) == value)
					pass = true; break;
			case NEQUAL:
				if((*it) != value)
					pass = true; break;
			case LESS_THAN: 
			case GREATER_THAN:
			case LESS_EQUAL:
			case GREATER_EQUAL:
				numeric_value = convert(model.getMapString(role, *it));
//				Logger::message("valore ricevuto dalla mappa: "+model.getMapString(role, *it));
				switch (op) {
				case LESS_THAN:
					if(numeric_value < compare_value)
						pass = true; break;
				case GREATER_THAN:
					if(numeric_value > compare_value)
						pass = true; 
					ss << numeric_value.int_value << " > " << compare_value.int_value;
//					Logger::message("espressione: "+ss.str());
					ss.str("");
					break;
				case LESS_EQUAL:
					if(numeric_value <= compare_value)
						pass = true; break;
				case GREATER_EQUAL:
					if(numeric_value >= compare_value)
						pass = true;
				}
				break;
			default:
				Logger::message("operazione non valida");
			}
			if(pass) {
				updatedTemp.first.push_back(*it);
			}
		} break;
	case SUM:
	case AVG:
	case MIN:
	case MAX:
		for(it=tempResult.first.begin(); it!=tempResult.first.end(); it++) {
			numeric_value = convert(model.getMapString(role, *it));
			numeric_values.push_back(numeric_value);
			if(min.type==UNDEFINED || min>numeric_value) 
				min = numeric_value;
			if(max.type==UNDEFINED || max<numeric_value)
				max = numeric_value;
			sum = sum + numeric_value;
			occ++;
		}
		updatedTemp.second = 0;
		switch (op) {
		case SUM:
			c_value = sum; break;
		case AVG:
			c_value = sum/occ; break;
		case MIN:
			c_value = min; break;
		case MAX:
			c_value = max; break;
		}
	}
	return TempResolutions(variable, Solutions(updatedTemp, c_value));
}

void QueryFilters::add(const Filter &f) {
	filters.push_back(f);
}

void QueryFilters::apply(QueryVariables &vars, const Model &model) {
	std::vector<Filter>::iterator it;
	for(it=filters.begin(); it!=filters.end(); it++) {
		TempResolutions res = applyFilter(*it, vars, model);
		if(res.first != 0 && res.second.first.second != 0) {
			vars.updateSolvedVariable(res.first, res.second.first);
		}
	}
}

QueryFilters::QueryFilters() {
	string2opt["UNKNOWN"] = UNKNOWN;
	string2opt["and"] = AND;
	string2opt["or"] = OR;
	string2opt["eq"] = EQUAL;
	string2opt["neq"] = NEQUAL;
	string2opt["lt"] = LESS_THAN;
	string2opt["gt"] = GREATER_THAN;
	string2opt["le"] = LESS_EQUAL;
	string2opt["ge"] = GREATER_EQUAL;
	string2opt["sum"] = SUM;
	string2opt["avg"] = AVG;
	string2opt["min"] = MIN;
	string2opt["max"] = MAX;
}
