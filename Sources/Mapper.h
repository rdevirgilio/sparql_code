/**
\class Mapper

\brief Provide the mapping of subjects, predicates and objects

This class main function is to map string values of subjects, predicate and objects to their
respective long representation used in the model.
*/
#ifndef __MAPPER_H__
#define __MAPPER_H__

#include <string>
#include <map>

typedef std::map<std::string, unsigned long> string2value;

class Mapper {
	private:
		unsigned long i,j,k;
		string2value subjects;
		string2value predicates;
		string2value objects;
	public:
		/** simple constructor */
		Mapper();
		/** it returns the respective map */
		string2value getMap(const char &c);
		/** this method is used in order to update the map in case of loading */
		void setStringAndValue(const char &c, const std::string &s, const unsigned long &l);
		/** gets the long value associated to a string in the subjects map*/
		unsigned long addSubject(const std::string &s);
		/** gets the long value associated to a string in the predicates map*/
		unsigned long addPredicate(const std::string &p);
		/** gets the long value associated to a string in the objects map*/
		unsigned long addObject(const std::string &o);
		/** gets size of subjects, predicates and objects*/
		unsigned long getSize(const char &c) const;
		/** gets the string associated to unsigned long value in the subjects domain*/
		std::string getSubject(const unsigned long &value) const;
		/** gets the string associated to unsigned long value in the predicates domain*/
		std::string getPredicate(const unsigned long &value) const;
		/** gets the string associated to unsigned long value in the objects domain*/
		std::string getObject(const unsigned long &value) const;
		/** finds a value in the maps*/
		unsigned long finds(char c, const std::string &s) const;
};

#endif
