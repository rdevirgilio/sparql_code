#include "TensorSPARQL.h"

TensorSPARQL::TensorSPARQL() {
	process = 1;
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
}

void TensorSPARQL::set(unsigned long * dims) {
	dimensions = dims;
	unsigned long sizes[6];
	sizes[0] = 0; sizes[1] = 0; sizes[2] = 0; 
	sizes[3] = dimensions[0]; sizes[4] = dimensions[1]; sizes[5] = dimensions[2];
}

bool TensorSPARQL::present(const Triple &element) const {
	Tuple t; 
	bool result = false;
	int res = 0;
	int* results = new int[numtasks];
	t.setValue(SUBJECT, element[SUBJECT]);
	t.setValue(PREDICATE, element[PREDICATE]);
	t.setValue(OBJECT, element[OBJECT]);
	unsigned long tuple[4];
	t.getArray(tuple);
	tuple[3] = 1;
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(&tuple, 4, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gather(&res, 1, MPI_INT, results, 1, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	for(int i=0; i<numtasks; i++) {
		if(results[i] != 0)
			result = true;
	}
	delete[] results;
	return result;
}

std::vector<Tuple> TensorSPARQL::getElements(Tuple &t, unsigned long opt) const{
	std::vector<Tuple> elements;
	unsigned long res[3], tuple[4];
	t.getArray(tuple);
	tuple[3] = opt;
	int* sizes = new int[numtasks];
	int size = 0;
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(&tuple, 4, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	int* recv_counts = new int[numtasks];
	int* displs = new int[numtasks];
	for(int i=0; i<numtasks; i++) {
		recv_counts[i] = 1;
		displs[i] = i;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gatherv(&size, 1, MPI_INT, sizes, recv_counts, displs, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	for(int i=0; i<numtasks; i++) {
		size = size + sizes[i];
//		std::cout << i << ": size ritornato " << sizes[i] << std::endl; 
	}
//	std::cout << "la size complessiva e' di " << size << std::endl;
	delete[] recv_counts;
	delete[] displs;
	int* disps = new int[numtasks];
	for(int i=0; i<numtasks; i++) {
		if(i!=0)
			disps[i] = disps[i-1]+sizes[i-1]*3;
		else disps[i] = i;
	}
	for(int i=0; i<numtasks; i++)
		sizes[i] = sizes[i]*3;
/*	for(int i=0; i<numtasks; i++) {
		std::cout << i << ": " << disps[i] << " " << sizes[i] << std::endl;
	} */
	unsigned long* tuples = new unsigned long[3*size];
	unsigned long* value;
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Gatherv(value, 0, MPI_UNSIGNED_LONG, tuples, sizes, disps, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	for(int i=0; i<size; i++) {
		res[0] = tuples[3*i];
		res[1] = tuples[3*i+1];
		res[2] = tuples[3*i+2];
		Tuple ti(res);
		elements.push_back(ti);
	}
	delete[] disps;
	delete[] sizes;
	delete[] tuples;
/*	for(int i=0; i<elements.size(); i++)
		elements[i].print(); */
	return elements;
}

std::vector<Tuple> TensorSPARQL::getDelta(const Delta &delta) const {
	Tuple tuple;
	tuple(0, 0, 0);
	tuple.setValue(delta.first, delta.second);
	return getElements(tuple, 2);
}

std::vector<Tuple> TensorSPARQL::getDoubleDelta(const Delta &d1, const Delta &d2) const {
	std::vector<Tuple> result;
	Tuple tuple;
	tuple(0, 0, 0);
	tuple.setValue(d1.first, d1.second);
	tuple.setValue(d2.first, d2.second);
	return getElements(tuple, 2);
}

std::vector<Tuple> TensorSPARQL::all() const{
	std::vector<Tuple> result;
	Tuple tuple;
	tuple(0, 0, 0);
	return getElements(tuple, 3);
}

void TensorSPARQL::printAll() {
	std::vector<Tuple> NZE = all();
	std::vector<Tuple>::iterator it;
	for(it=NZE.begin(); it!=NZE.end(); it++) {
		(*it).print();
	}
	std::cout << std::endl;
}