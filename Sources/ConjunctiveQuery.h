/**
\class ConjunctiveQuery

\brief provide a representation of conjunctive query

This class is meant to represent conjunctive query and it realizes the execution of the query.
*/

#ifndef __CONJUNCTIVEQUERY_H__
#define __CONJUNCTIVEQUERY_H__

#include "Triple.h"
#include "QueryTriples.h"
#include "QueryVariables.h"
#include "QueryFilters.h"
#include "Model.h"
#include <utility>
#include <map>
#include <set>

class ConjunctiveQuery {
	private:
		QueryTriples data;
		QueryVariables vars;
		QueryFilters filters;
		/** it handles the execution of patterns which degree of freedom is equal to 3 */
		bool caseThree(const Triple &t, Resolutions S, Resolutions P, Resolutions O, const Model &model);
		/** it handles the execution of patterns which degree of freedom is equal to 1 */
		bool caseOne(const Triple &t, Resolutions S, Resolutions P, Resolutions O, const Model &model);
		/** it handles the execution of patterns which degree of freedom is equal to -1 */
		bool caseMinusOne(const Triple &t, Resolutions S, Resolutions P, Resolutions O, const Model &model);
		/** it's responsable of the application of the pattern t on the Tensor mat and handles also the case od DOF = -3 */
		bool application(const Triple &t, const Model &model);
	public:
		/** constructor which takes in input a QueryTriples and QueryVariables objects */
		ConjunctiveQuery(const QueryTriples &data, const QueryVariables &vars, const QueryFilters &fs);
		/** this function is called whenever an execution of a query is needed and
			returns a map of select variables and their values*/
		void execute(const Model &model, Result &res);
};

#endif
