/**
\class TensorPart

\brief Provide Tuples handle in any single process

This class main function is to represent a Tensor part, filled with tuples scattered
in the preprocessing.
*/
#ifndef __TENSORPART_H__
#define __TENSORPART_H__

#include "Tuple.h"
#include "TensorSPARQL.h"
#include <vector>

class TensorPart {
	private:
		std::vector<Tuple> NonZeroElements;
	public:
		/** adds a tuple t to the TensorPart */
		void add(const Tuple &t);
		/** verify if a tuple t is present or not in TensorPart */
		bool isPresent(const Triple &t);
		/** gets a vector of tuples, which suits the Delta passed as parameter */
		std::vector<Tuple> getDelta(const Tuple &t);
		/** gets all the tuples stored in TensorPart */
		std::vector<Tuple> all();
		/** prints all the tuples */
		void printAll(int rank);
};

#endif