/**
\class Logger

\brief provide debug logging

This class's main purpose is to print on screen debug messages
*/

#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <string>
#define null 0

class Logger {
	private:
		static Logger* logger;
		Logger& operator=(const Logger &log) {};
		Logger(const Logger &log) {};
		Logger() {};
	public:
		/** it enables to print debug message only in debug mode */
		static void message(const std::string &s);
};

#endif