#include "QueryTriples.h"
#include <vector>
#include <list>
#include "Triple.h"
#include "QueryVariables.h"
#include <iostream>

QueryTriples::QueryTriples() {
	ONE = MINUSONE = MINUSTHREE = triples.end();
}

void QueryTriples::add(Triple t, short setting) {
	char dof;
	if(setting == 0)
		dof = t.getDegreeOfFreedom();
	else dof = vars->DOF(t);
	switch (dof) {
	case 3:
		triples.push_front(t); break;
	case 1:
		if(ONE != MINUSONE) {
			triples.insert(MINUSONE, t);
		}
		else {
			triples.insert(MINUSONE, t);
			ONE--;
		}
		break;
	case -1:
		if(MINUSONE != MINUSTHREE) {
			triples.insert(MINUSTHREE, t);
		}
		else {
			triples.insert(MINUSTHREE, t);
			MINUSONE--;
		}
		break;
	case -3:
		triples.push_back(t);
	}
}

void QueryTriples::setData(std::vector<Triple> data) {
	std::vector<Triple>::iterator it;
	for(it=data.begin(); it!=data.end(); it++)
		add(*it,0);
}

void QueryTriples::setVars(QueryVariables * v) {
	vars = v;
}

Triple QueryTriples::getPattern() {
	Triple pattern = triples.front();
	triples.erase(triples.begin());
	processedPatterns.push_front(pattern);
	return pattern;
}

unsigned int QueryTriples::getSize() {
	return triples.size();
}

bool QueryTriples::isEmpty() {
	return triples.size()==0;
}

bool QueryTriples::arranged() {
	char dof = 3;
	Triple t;
	std::list<Triple>::iterator it;
	for(it=triples.begin(); it!=triples.end(); it++) {
		if(it==ONE)
			dof = 1;
		if(it==MINUSONE)
			dof = -1;
		if(it==MINUSTHREE)
			dof = -3;
		if(vars->DOF(*it)!=dof) {
			t = *it;
			triples.erase(it);
			add(t);
			return false;
		}
	}
	return true;
}

void QueryTriples::update() {
	while(!arranged()) {}
}

std::list<Triple> QueryTriples::getProcessedPatterns() {
	return processedPatterns;
}