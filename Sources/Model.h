/**
\class Model

\brief Provide representation of the dataset

This class main function is to bind the data representation with the respective mapping
*/
#ifndef __MODEL_H__
#define __MODEL_H__

#include "Mapper.h"
#include "TensorSPARQL.h"
#include "QueryVariables.h"
#include <vector>

class Model {
	private:
		Mapper mapper;
		TensorSPARQL tensor;
	public:
		/** constructor which take as parameter the uri of the file as a std::string */
		Model(const std::string &file_uri);
		/** method that load the model data from a binary file */
		void load(const std::string &fname);
		/** gets all Tuple inside the tensor */
		std::vector<Tuple> getAllTensorData() const;
		/** gets as input a Delta object and returns all the tuples of kronecker's product between 
		model's tensor and the Delta */
		std::vector<Tuple> getDelta(const Delta &v) const;
		/** gets in input two Deltas and returns the result of two consecutive kronecker's product */
		std::vector<Tuple> getDoubleDelta(const Delta &v1, const Delta &v2) const;
		/** tests if a Triple is contained in model's tensor */
		bool presentInTensor(const Triple &t) const;
		/** gets the respective string from an unsigned long value in the model's mapper object */
		std::string getMapString(const char &role, const unsigned long &value) const;
		/** gets the unsigned long related to a string inside the Mapper */
		unsigned long findInMap(const char &c, const std::string &s) const;
		/** converts temporaly solution from a domain to another */
		Resolutions conversion(char i, const Resolutions &res) const;
		/** shows the content of a map */
		void showMap(const char c);
		/** shows content of the tensor */
		void printTensor();
};

#endif