/**
\class Tuple

\brief Provide a representation of NonZeroElement of the tensor

this class is meant to store NonZeroElement's coordinate in the data structure
*/
#ifndef __Tuple_H__
#define __Tuple_H__

#include <iostream>
#include "Logger.h"
#include "Triple.h"

class Tuple {
	private:
		unsigned long i;
		unsigned long j;
		unsigned long k;
	public:
		Tuple() {};
		/** construct a Tuple with the first three element of the array passed */
		Tuple(unsigned long* array) {
			i = array[0];
			j = array[1];
			k = array[2];
		};
		/** it gets the unsigned long value in t_role position in the tuple */
		unsigned long operator[](const char &t_role) const{
			switch (t_role) {
			case SUBJECT: return i;
			case PREDICATE: return j;
			case OBJECT: return k;
			default: Logger::message("Tuple->operator[]: caso non gestito");
				return 0;
			}
		};
		/** it sets the unsigned long value in the t_role position in the tuple */
		void setValue(const char &t_role, const unsigned long &value) {
			switch (t_role) {
			case SUBJECT: i = value; break;
			case PREDICATE: j = value; break;
			case OBJECT: k = value; break;
			default: Logger::message("Tuple->setValue: caso non gestito"); 
			}
		};
		/** compares tuple with a Triple pattern (DOF = 3) */
		bool operator==(const Triple &element) const {
			return i==element[SUBJECT] && j==element[PREDICATE] && k==element[OBJECT];
		};
		void print() const{
			std::cout << "<" << i << " " << j << " " << k << ">" << " " << std::endl;
		};
		/** a fast setter for the tuple */
		void operator()(unsigned long a, unsigned long b, unsigned long c) {
             i = a; j = b; k = c;
        };
		/** fills the first three position of the passed array with Tuple content */
		void getArray(unsigned long tuple[]) const{
			tuple[0] = i;
			tuple[1] = j;
			tuple[2] = k;
		};
};

#endif
