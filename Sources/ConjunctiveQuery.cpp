#include "ConjunctiveQuery.h"
#include "Logger.h"
#include <vector>
#include <sstream>
#include <algorithm>

ConjunctiveQuery::ConjunctiveQuery(const QueryTriples &triples, const QueryVariables &variables, const QueryFilters &fs) {
	data = triples;
	vars = variables;
	filters = fs;
}

SolvedValues difference(SolvedValues &a, SolvedValues &b) {
	SolvedValues output;
	if(b.size() == 0)
		return a;
	if(a.size() == 0)
		return a;
	std::sort(a.begin(), a.end());
	std::sort(b.begin(), b.end());
	std::set_difference(a.begin(), a.end(), b.begin(), b.end(), std::back_inserter(output));
	return output; 
}

SolvedValues unify(SolvedValues &a, SolvedValues &b) {
	if(b.size() == 0)
		return a;
	if(a.size() == 0)
		return b;
	SolvedValues output;
	std::sort(a.begin(), a.end());
	std::sort(b.begin(), b.end());
	set_union(a.begin(), a.end(), b.begin(), b.end(), inserter(output, output.begin()));
//	std::cout << output.size() << std::endl;
	return output; 
}

void getValues(SolvedValues &s, const char &i, const std::vector<Tuple> &tuples) {
	std::vector<Tuple>::const_iterator it;
	for(it=tuples.begin(); it!=tuples.end(); it++)
		switch (i) {
		case SUBJECT:
			s.push_back((*it)[SUBJECT]); break;
		case PREDICATE:
			s.push_back((*it)[PREDICATE]); break;
		case OBJECT:
			s.push_back((*it)[OBJECT]); break;
		}
}

bool ConjunctiveQuery::caseThree(const Triple &t, Resolutions S, Resolutions P, Resolutions O, const Model &model) {
	Resolutions Ds; Ds.second = SUBJECT;
	Resolutions Dp; Dp.second = PREDICATE;
	Resolutions Do; Do.second = OBJECT;
	Triple t1;
	SolvedValues::iterator its,itp,ito;
	for(its=S.first.begin(); its!=S.first.end(); ++its)
		for(itp=P.first.begin(); itp!=P.first.end(); ++itp)
			for(ito=O.first.begin(); ito!=O.first.end(); ++ito) {
				t1(*its,*itp,*ito);
				if(model.presentInTensor(t1)) {
					Ds.first.push_back(*its);
					Dp.first.push_back(*itp);
					Do.first.push_back(*ito);
				}
				else {
					Ds.first.erase(std::remove(Ds.first.begin(), Ds.first.end(), *its), Ds.first.end());
					Dp.first.erase(std::remove(Dp.first.begin(), Dp.first.end(), *itp), Dp.first.end());
					Do.first.erase(std::remove(Do.first.begin(), Do.first.end(), *ito), Do.first.end());
				}
			}
	S.first = difference(S.first,Ds.first);
	P.first = difference(P.first,Dp.first);
	O.first = difference(O.first,Do.first);
	if(t[SUBJECT]<0) vars.updateSolvedVariable(t[SUBJECT], S);
	if(t[PREDICATE]<0) vars.updateSolvedVariable(t[PREDICATE], P);
	if(t[OBJECT]<0) vars.updateSolvedVariable(t[OBJECT], O);
	return S.first.size()!=0 && P.first.size()!=0 && O.first.size()!=0;
}

bool ConjunctiveQuery::caseOne(const Triple &t, Resolutions S, Resolutions P, Resolutions O, const Model &model) {
	Resolutions X; X.second = 0;
	SolvedValues X_sols;
	SolvedValues::iterator its, itp, ito;
	switch (vars.roleVariable(t)) {
	case SUBJECT:
		for(itp=P.first.begin(); itp!=P.first.end(); itp++)
			for(ito=O.first.begin(); ito!=O.first.end(); ito++) {
				Delta vect1(PREDICATE,*itp);
				Delta vect2(OBJECT,*ito);
				getValues(X_sols,SUBJECT,(model.getDoubleDelta(vect1, vect2)));
				X.first = unify(X.first, X_sols);
			}
		X.second = SUBJECT; 
		vars.updateSolvedVariable(t[SUBJECT], X); break;
	case PREDICATE:
		for(its=S.first.begin(); its!=S.first.end(); its++)
			for(ito=O.first.begin(); ito!=O.first.end(); ito++) {
				Delta vect1(SUBJECT,*its);
				Delta vect2(OBJECT,*ito);
				getValues(X_sols,PREDICATE,(model.getDoubleDelta(vect1, vect2)));
				X.first = unify(X.first, X_sols);
			}
		X.second = PREDICATE;
		vars.updateSolvedVariable(t[PREDICATE], X); break;
	case OBJECT:
		for(its=S.first.begin(); its!=S.first.end(); its++)
			for(itp=P.first.begin(); itp!=P.first.end(); itp++) {
				Delta vect1(SUBJECT,*its);
				Delta vect2(PREDICATE,*itp);
				getValues(X_sols,OBJECT,(model.getDoubleDelta(vect1, vect2)));
				X.first = unify(X.first, X_sols);
			}
		X.second = OBJECT;
		vars.updateSolvedVariable(t[OBJECT], X); break;
	default:
		return false;
	}
	return X.first.size()!=0;
}

bool ConjunctiveQuery::caseMinusOne(const Triple &t, Resolutions S, Resolutions P, Resolutions O, const Model &model) {
	Resolutions E1;
	Resolutions E2;
	SolvedValues E1_sols;
	SolvedValues E2_sols;
	SolvedValues::iterator it;
	switch (vars.roleCostant(t)) {
	case SUBJECT:
		for(it=S.first.begin(); it!=S.first.end(); it++) {
			Delta vect(SUBJECT,*it);
			std::vector<Tuple> res = model.getDelta(vect);
			std::cin.get();
			getValues(E1_sols,PREDICATE,res);
			getValues(E2_sols,OBJECT,res);
			E1.first = unify(E1.first, E1_sols);
			E2.first = unify(E2.first, E2_sols);
		}
		E1.second = PREDICATE; E2.second = OBJECT;
		vars.updateSolvedVariable(t[PREDICATE], E1);
		vars.updateSolvedVariable(t[OBJECT], E2); break;
	case PREDICATE:
		for(it=P.first.begin(); it!=P.first.end(); it++) {
			Delta vect(PREDICATE,*it);
			std::vector<Tuple> res = model.getDelta(vect);
			getValues(E1_sols,SUBJECT,res);
			getValues(E2_sols,OBJECT,res);
			E1.first = unify(E1.first, E1_sols);
			E2.first = unify(E2.first, E2_sols);
		}
		E1.second = SUBJECT; E2.second = OBJECT;
		vars.updateSolvedVariable(t[SUBJECT], E1);
		vars.updateSolvedVariable(t[OBJECT], E2); break;
	case OBJECT:
		for(it=O.first.begin(); it!=O.first.end(); it++) {
			Delta vect(OBJECT,*it);
			std::vector<Tuple> res = model.getDelta(vect);
			getValues(E1_sols,SUBJECT,res);
			getValues(E2_sols,PREDICATE,res);
			E1.first = unify(E1.first, E1_sols);
			E2.first = unify(E2.first, E2_sols);
		}
		E1.second = SUBJECT; E2.second = PREDICATE;
		vars.updateSolvedVariable(t[SUBJECT], E1);
		vars.updateSolvedVariable(t[PREDICATE], E2); break;
	default:
		return false;
	}
	return E1.first.size()!=0 && E2.first.size()!=0;
}

bool ConjunctiveQuery::application(const Triple &t, const Model &model) {
	Resolutions S; S.second = SUBJECT;
	Resolutions P; P.second = PREDICATE;
	Resolutions O; O.second = OBJECT;
	if(t[SUBJECT]<0)
		if(S.second==vars.getSolvedVariable(t[SUBJECT]).second)
			S.first = vars.getSolvedVariable(t[SUBJECT]).first;
		else { 
			S = model.conversion(SUBJECT,vars.getSolvedVariable(t[SUBJECT]));
			vars.updateSolvedVariable(t[SUBJECT], S);
		}
	else S.first.push_back(t[SUBJECT]);
	if(t[PREDICATE]<0)
		if(P.second==vars.getSolvedVariable(t[PREDICATE]).second)
			P.first = vars.getSolvedVariable(t[PREDICATE]).first;
		else {
			P = model.conversion(PREDICATE,vars.getSolvedVariable(t[PREDICATE]));
			vars.updateSolvedVariable(t[PREDICATE], P);
		}
	else P.first.push_back(t[PREDICATE]);
	if(t[OBJECT]<0)
		if(O.second==vars.getSolvedVariable(t[OBJECT]).second)
			O.first = vars.getSolvedVariable(t[OBJECT]).first;
		else {
			O = model.conversion(OBJECT,vars.getSolvedVariable(t[OBJECT]));
			vars.updateSolvedVariable(t[OBJECT], O);
		}
	else O.first.push_back(t[OBJECT]);
	switch (vars.DOF(t)) {
	case 3:
		return caseThree(t,S,P,O,model);
	case 1:
		return caseOne(t,S,P,O,model);
	case -1:
		return caseMinusOne(t,S,P,O,model);
	case -3:
		getValues(S.first,SUBJECT,model.getAllTensorData());
		getValues(P.first,PREDICATE,model.getAllTensorData()); 
		getValues(O.first,OBJECT,model.getAllTensorData()); 
		vars.updateSolvedVariable(t[SUBJECT],S);
		vars.updateSolvedVariable(t[PREDICATE],P);
		vars.updateSolvedVariable(t[OBJECT],O);
		return true;
	default:
		return false;
	}
	return true;
}

void ConjunctiveQuery::execute(const Model &model, Result &res) {
	bool proceed = true;
	Triple t;
	int i = 0;
	Logger::message("esecuzione della query");
	while(!data.isEmpty() && proceed) {
		t = data.getPattern();
		std::stringstream ss;
		ss << "iterazione " << i << ":";
		Logger::message(ss.str());
		Logger::message("esecuzione del pattern: " + t.toString());
		proceed = application(t,model);
		i++;
		filters.apply(vars, model);
		if(proceed)
			Logger::message("fine iterazione");
		else Logger::message("query fallita");
	}
	vars.getResult(res);
}