/**
\class Triple

\brief Provide a representation of either graph triples and query triple patterns

This class represents both graph triple and triple pattern, it's the basic element of the model.
*/

#ifndef _ROLE_
#define _ROLE_
enum role {
	SUBJECT = 1,
	PREDICATE = 2,
	OBJECT = 3
};
#endif

#ifndef __TRIPLE_H__
#define __TRIPLE_H__

#include <iostream>
#include <sstream>
#include "Logger.h"

class Triple {
	private:
		long s;
		long p;
		long o;
	public:
		/** method that return a long in subject, predicate or a object position */
		long operator[](const char &t_role) const {
			switch (t_role) {
			case SUBJECT: return s;
			case PREDICATE: return p;
			case OBJECT: return o;
			default: Logger::message("Triple->operator[]: caso non gestito");
				return 0;
			}
		};
		/** it sets a long value in t_role position in the triple */
		void setValue(const char &t_role, long value) {
			switch (t_role) {
			case SUBJECT: s = value; break;
			case PREDICATE: p = value; break;
			case OBJECT: o = value; break;
			default: Logger::message("Triple->setValue: caso non gestito");
			}
		}
		/** returns the DOF of the pattern */
		char getDegreeOfFreedom() {
			char dof = 0;
			if(s<0)
				dof--;
			else dof++;
			if(p<0)
				dof--;
			else dof++;
			if(o<0)
				dof--;
			else dof++;
			return dof;
		};
		/** this function allows to set values to our triple */
		void operator()(long x, long y, long z) {
			s = x; p = y; o = z;
		};
		/** returns a string with triple values */
		std::string toString() {
			std::ostringstream str; 
			str << "<" << s << " " << p << " " << o << ">";
			return str.str();
		};
		/** gets the constant position in the triple in case of DOF = -1 */
		char getRoleCostant() {
			if(getDegreeOfFreedom()!=-1) {
				Logger::message("non e' possibile trovare una posizione sola per la costante");
				return 0;
			}
			else {
				if(s>0)
					return SUBJECT;
				if(p>0)
					return PREDICATE;
				if(o>0)
					return OBJECT;
			}
		}
};

#endif
